ARG PHP_VERSION=8.2.9
ARG NGINX_VERSION=1.19.3

FROM php:${PHP_VERSION}-fpm-alpine as builder
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions  /usr/local/bin/
COPY /supported-extensions /supported-extensions
COPY /install-extensions /install-extensions

RUN chmod +x /usr/local/bin/install-php-extensions && sync
RUN chmod +x /install-extensions;

RUN apk add zlib-dev; exit 0
RUN apk add libzip-dev; exit 0
RUN apk add libmemcached-dev; exit 0
RUN apk add freetype-dev; exit 0
RUN apk add openssl-dev; exit 0
RUN apk add libressl-dev; exit 0
RUN apk add pkgconfig; exit 0
RUN apk add bash; exit 0

RUN /install-extensions ${PHP_VERSION}

RUN PHP_EXT=$(find $(php -d 'display_errors=stderr' -r 'echo ini_get("extension_dir");')   -maxdepth 1 -type f -name '*.so' -exec basename '{}' ';' | sort  | xargs);  \
    docker-php-ext-enable $PHP_EXT; \
    docker-php-source delete \
    ;

COPY /php-binary /usr/local/bin
COPY /docker-php-entrypoint /docker-healthcheck /binary-install /binary-remove /docker/
RUN chmod +x /usr/local/bin/php-binary; \
    chmod +x /docker/binary-install; \
    chmod +x /docker/binary-remove; \
    chmod +x /docker/docker-php-entrypoint; \
    chmod +x /docker/docker-healthcheck; \
    php-binary \
    ;
#
FROM nginx:${NGINX_VERSION}-alpine as nginx
COPY /nginx-binary /usr/local/bin
RUN apk add binutils util-linux; \
    chmod +x /usr/local/bin/nginx-binary; \
    nginx-binary \
    ;

FROM alpine
COPY --from=builder /binary /binary
COPY --from=builder /docker /usr/local/bin
COPY --from=nginx /binary /binary


